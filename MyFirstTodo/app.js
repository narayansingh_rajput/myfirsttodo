/*eslint-env node*/

//------------------------------------------------------------------------------
// node.js starter application for Bluemix
//------------------------------------------------------------------------------

// This application uses express as its web server
// for more info, see: http://expressjs.com
var express = require('express');
var _ = require('underscore');
var connect = require('connect');
var cons = require('consolidate');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var ibmbluemix = require('ibmbluemix');
var ibmcloudcode = require('ibmcloudcode');
var http =require('http');
var ibmbluemix = require('ibmbluemix');
var config = {
  applicationId:"889772a6-4e6e-11e5-885d-feff819cdc9f",
  applicationRoute:"mytodoappsample"
};
ibmbluemix.initialize(config);

// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
var cfenv = require('cfenv');

// create a new express server
var app = express();

// serve the files out of ./public as our main files
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view options', {layout: false});
app.engine('html', cons.underscore);
app.set('view engine', 'html');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride());
var count =0;

app.get('/index',function(req,res){
  count++;
	res.render('index',{"name":""});
});

app.get('/*',function(req,res){
	res.redirect('index');
});

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();

// start server on the specified port and binding host
var server =http.createServer(app).listen(appEnv.port, '0.0.0.0', function() {
	// print a message when the server starts listening
  console.log("server starting on " + appEnv.url);
});



var io = require('socket.io').listen(server,{ log: false });

io.sockets.on('connection', function (socket) {
	socket.leave(socket.id);
    socket.join('Default');
	socket.on('username',function(data){
		socket.username=data.name;
		var messageString = socket.username+' has joined the room '; 
        io.sockets.to('Default').emit('joinedUser',{'message':messageString});
		socket.emit("userstat",{'name':data.name,'count':count});
	});	

	socket.on('message',function(data){
        socket.broadcast.to('Default').emit('message',{"name":socket.username,"msg":data.msg});
    });

    socket.on('disconnect', function(){
        if(socket.id){
            var messageString = socket.username+' has left the room';
            socket.broadcast.to('Default').emit('leftRoom',{"message":messageString});
        }
    });
});